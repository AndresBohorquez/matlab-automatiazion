m1=[0 1 2; 5 -7 4; -2 11 -3]%declare matrix
m1T=m1' %matrix transpose

complexMatrix=[1-j 5+j; 1 3j]
complexMatrixT=complexMatrix.' %matrix transpose of a complex matrix

m2=[0 1 0; 0 0 1; -4 -3 -5]
sumM1M2=m1+m2

productM1M2=m1*m2%matrix multiplication
productM1M2=m1.*m2%matrix point to point multiplication

divideM1M2=m1./m2%pointto point division
divideM2M1=m1.\m2

absolutevalueM1=abs(m1)

invM1=inv(m1)

eigM2=eig(m2)%those two are the same only if is in the form viewed in class
rootsM2=roots([1 5 3 4])

rootsM1=eig(m1)
rootsM2=eig(m2)

timeStep=-4:0.03:4
timeStep2=linspace(-10,10,8)%from -10 to 10 8 steps

v1=[1 5 3 4]%vector
v1(2)%2nd value of vector
polinomiumM2=poly(m2)

polA=[1 10 2]
polB=[1 11 -4 -3 0]
polAxB=conv(polA,polB)
polAin2=polyval(polA,2)
[product,residue]=deconv(polB,polA)

I=eye(10)
oneM=ones(10)
diag(1:5)%make a diagonal matrix with that tmestep

plot(timeStep,sin (timeStep*pi), timeStep, cos(timeStep*pi))
hold on
plot(timeStep,sin(timeStep*pi+1),':o')
grid on
text(3,0.2,'.a')

num=[1,2]
den=[1,3,5,1,2]
%a is what follows x xpoint
%b is what follows U xpoint
%c is waht follows x in y
%d is what follows U in y
[A,B,C,D]=tf2ss(num,den)
A=[0 1 0 0;0 0 1 0; 0 0 0 1; -8 -1 -10 -2]
rootsA=eig(A)
C=[10 5 0 0]
B=[0;0;0;1]
D=0
rootsC= roots(C)
[num1,den1]=ss2tf(A,B,C,D)
zeros=roots([0,0,0,5,10])
